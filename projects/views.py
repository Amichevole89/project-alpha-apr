from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectCreateForm

# from django.shortcuts import get_object_or_404


@login_required
def ProjectView(request):
    project_items = Project.objects.filter(members=request.user)
    context = {"projectList": project_items}
    return render(request, "project/list.html", context)


@login_required
def ProjectDetail(request, pk):
    projectDetail = Project.objects.get(pk=pk)
    context = {"projectDetail": projectDetail}
    return render(request, "project/detail.html", context)


# @login_required
# def ProjectCreate(request):
#     if request.method == "POST":
#         form = ProjectCreateForm(request.POST)
#         if form.is_valid():
#             item = form.save()
#             item.members.set([request.user.pk])

#             return redirect("home")
#     else:
#         form = ProjectCreateForm()
#     context = {"form": form}
#     return render(request, "projects/create.html", context)


@login_required
def ProjectCreate(request):
    if request.method == "POST":
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_project", item.id)
    else:
        form = ProjectCreateForm()
    context = {"form": form}
    return render(request, "project/create.html", context)
