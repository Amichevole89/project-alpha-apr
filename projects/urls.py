from django.urls import path
from projects.views import ProjectView, ProjectDetail, ProjectCreate

urlpatterns = [
    path("", ProjectView, name="list_projects"),
    path("<int:pk>/", ProjectDetail, name="show_project"),
    path("create/", ProjectCreate, name="create_project"),
]
