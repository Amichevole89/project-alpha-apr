from django.forms import ModelForm
from projects.models import Project


class ProjectCreateForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "members"]
