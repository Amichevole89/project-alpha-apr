from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskCreateForm, TaskUpdateForm
from tasks.models import Task


@login_required
def TaskCreate(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            # sets post_pk to the id number in the post
            post_pk = request.POST.get("project", "/")
            # redirects to current app detail view for post at post_pk
            return redirect("http://localhost:8000/projects/" + post_pk)
    else:
        form = TaskCreateForm()
    context = {"form": form}
    return render(request, "task/create.html", context)


@login_required
def TaskView(request):
    task_items = Task.objects.filter(assignee=request.user)
    context = {"task_items": task_items}
    return render(request, "task/list.html", context)


@login_required
def TaskUpdate(request, pk):
    task_update = Task.objects.get(pk=pk)
    if request.method == "POST":
        form = TaskUpdateForm(request.POST, instance=task_update)
        if form.is_valid():
            task_update = form.save(commit=False)
            task_update.is_completed = True
            task_update.save()
            return redirect("show_my_tasks")
    else:
        form = TaskUpdateForm(instance=task_update)
    context = {"form": form}
    return render(request, "complete_task", context)
