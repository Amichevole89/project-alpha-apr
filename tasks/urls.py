from django.urls import path
from tasks.views import TaskCreate, TaskView, TaskUpdate

urlpatterns = [
    path("create/", TaskCreate, name="create_task"),
    path("mine/", TaskView, name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdate, name="complete_task"),
]
