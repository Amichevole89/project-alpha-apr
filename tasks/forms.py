from django.forms import ModelForm
from tasks.models import Task


class TaskCreateForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


class TaskUpdateForm(ModelForm):
    class Meta:
        model = Task
        fields = ["is_completed"]
